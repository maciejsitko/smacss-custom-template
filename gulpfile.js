// 0. Main Dependency 

var gulp = require('gulp');

// 1. Browser Sync/ Watch Tasks

var browsersync = require('browser-sync');
var watch = require('gulp-watch');

// 2. Styles:  Minification/Concatenation/Preprocessors

var concat = require('gulp-concat');
var imagemin = require('gulp-imagemin');
var minifycss = require('gulp-minify-css');
var csscomb = require('gulp-csscomb');
var autoprefixer = require('gulp-autoprefixer');
var sass = require('gulp-sass');
var uglify = require('gulp-uglify');
var preprocess = require('gulp-preprocess');
var postcss = require('gulp-postcss');
var prettify = require('gulp-prettify');
var scsslint = require('gulp-scss-lint');

// 3. Injections: Conditional Injection

var cond = require('gulp-if');
var inject = require('gulp-inject');

// 4. Utils

var newer = require('gulp-newer');
var notify = require('gulp-notify');
var plumber = require('gulp-plumber');
var merge = require('merge-stream'); 
var run = require('run-sequence');
var del = require('del');


var files = {};

files.src = {

	css:'app/css/**/*.scss',
	html: 'app/*.*',
	js : {
		dev: 'app/js/**/*.js',
		head: 'app/js/head.js',
		main: 'app/js/main.js'
	},
	img: 'img/**/*.*',
	syncDir: 'dev',
	watch: {
		html: 'app/*.html',
		css: 'app/css/**/*.scss',
		js: 'app/js/**/*.js',
		img: 'app/img/**/*.*'
	}

}


files.dest = {
	
	css: 'dev/css/',
	html: 'dev/',
	js: 'dev/js/',
	img: 'dev/img',
	dist: 'dist'

}

files.notify = {
	html: {
        errorHandler: notify.onError('VIEWS: BUILD FAILED!\n' +
        'Error:\n<%= error.message %>')
    },
    css: {
        errorHandler: notify.onError('STYLES: BUILD FAILED!\n' +
        'Error:\n<%= error.message %>')
    },
    js: {
        errorHandler: notify.onError('SCRIPTS: BUILD FAILED!\n' +
        'Error:\n<%= error.message %>')
    },
    img: {
        errorHandler: notify.onError('IMAGES: SOMETHING WRONG!\n' +
        'Error:\n<%= error.message %>')
    }

}




// 5. Tasks


// 5.1 Styles: Sass Preprocessing 


gulp.task('styles', function() {
   return gulp.src(files.src.css)
    .pipe(sass())
    .pipe(plumber(files.notify.css))
    .pipe(autoprefixer({
     	browsers: ['last 2 versions'],
	 	cascade: false
	 }))
    .pipe(gulp.dest('dev/css'))
     .pipe(browsersync.reload({stream: true}));
});

// 5.2 Scripts


gulp.task('js', function() {
  return gulp.src([files.src.js.dev, files.src.js.main, files.src.js.head])
  .pipe(plumber(files.notify.css))
  .pipe(uglify())
  .pipe(gulp.dest(files.dest.js))
  .pipe(browsersync.reload({stream: true}));

});

// 5.3 Html 

gulp.task('html', function() {
   return gulp.src(files.src.html)
   .pipe(plumber(files.notify.html))
   .pipe(prettify({indent_size: 2, optimizationLevel: 6, interlaced: true}))
   .pipe(gulp.dest(files.dest.html))
   .pipe(browsersync.reload({stream: true}));

});

// 5.4 Images

gulp.task('img', function () {
    return gulp.src(files.src.img)
        .pipe(plumber(files.notify.img))
        .pipe(newer(files.dest.img))
        .pipe(imagemin({progressive: true, optimizationLevel: 6, interlaced: true}))
        .pipe(gulp.dest(files.dest.img))
        .pipe(browsersync.reload({stream: true}));
});



// 6. Gulp Task Sequences

gulp.task('default', function() {
	run(['styles','html', 'js','img']);
});

gulp.task('dist', function() {

});

gulp.task('serve',['default'], function() {
	browsersync.init(null, {
		server: {
			baseDir: files.src.syncDir
		}
	})
});

// 7. Gulp Watches

watch(files.src.watch.css, function() {
	run('styles');
});

watch(files.src.watch.js, function() {
	run('js');
});

watch(files.src.watch.html, function() {
	run('html');
});

watch(files.src.watch.img, function() {
	run('img');
});






